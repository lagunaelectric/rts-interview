# RTS Interview

## Hello, Recruiter!

This is my implementation of your challenge in Python. Handling "equal" numbers wasn't specified, but I handled it just in case. I can't wait to hear back from you!

#### This project uses Python 3.9.5

- [Download](https://www.python.org/downloads/release/python-395/)
