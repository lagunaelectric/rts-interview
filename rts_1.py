import time


class RTS:
    def above_below(self, int_list: list[int], comparison_value: int) -> dict[str, int]:
        final_dict: dict[str, int] = {
            "above": 0,
            "below": 0,
            "equal": 0
        }

        for num in int_list:
            num_above = num > comparison_value
            num_below = num < comparison_value

            # What if it's equal? I added a case for that as well.
            relation: str = "above" if num_above else "below" if num_below else "equal"

            if relation not in final_dict.keys():
                final_dict[relation] = 0

            final_dict[relation] += 1

        return final_dict

    def string_rotation(self, in_string: str, rotation_amount: int) -> str:
        if rotation_amount < 0:
            raise ValueError("Rotation amount must be a positive integer.")

        # In case it's larger than the string we will ensure
        # it'll still work as intended. Also, good excuse
        # for a while loop!
        while rotation_amount > len(in_string):
            rotation_amount -= len(in_string)

        offset = 0 - rotation_amount

        return in_string[offset:] + in_string[:offset]


if __name__ == '__main__':
    # Feel free to put any test data you like here!
    rts = RTS()
    comp_val = 6
    int_list: list[int] = [4, 9, 3, 10, 3, 5, 2, 45, 7, 32, 8, 8]

    print(rts.above_below(int_list, comp_val))

    test_string = "123456789"
    test_amount = 0

    while test_amount < 1000:
        print(" " + rts.string_rotation(test_string, test_amount), end="\r", flush=True)
        test_amount += 1
        time.sleep(.1)
